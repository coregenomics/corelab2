#!/bin/bash

set -e -o pipefail

# Grub operations run long and are hard to validate, so allow skipping.
opts=$(getopt -o sr -l skip-grub,rescue -- "$@")
unset skip rescue
for opt in $opts
do
    case $opt in
	-s | --skip-grub ) skip=true ;;
	-r | --rescue ) rescue=true ;;
	* ) ;;
    esac
done
if [[ -n "$rescue" ]]
then
    # Also useful to do things like set root passwd ;)
    sudo zpool export -a
    sudo zpool import -N -R /mnt rpool
    sudo zpool import -N -R /mnt bpool
    sudo zfs mount rpool/ROOT/ubuntu
    sudo zfs mount -a
    exit 0
fi

#---------------------------------------------------------------------
# Setup remote connection

# Install emacs package.
if ! which emacs &> /dev/null
then
    sudo apt install --yes emacs25-nox
fi

# Install ssh server package for remote installation.
if ! which sshd &> /dev/null
then
    sudo apt install --yes openssh-server
    # Disable plain text login from sshd
    sudo systemctl stop sshd
fi

# Patch sshd config.
file=/etc/ssh/sshd_config
sudo sed -i -E \
     -e 's|.*(PermitRootLogin).*|\1 no|' \
     -e 's|.*(PasswordAuthentication).*|\1 no|' \
     $file
if ! grep -q AllowUsers $file
then
    sudo bash -c "echo AllowUsers ubuntu >> $file"
fi

# Enable configured sshd.
if ! systemctl is-active --quiet sshd
then
    sudo systemctl start sshd
fi

# Install tmux package.
if ! which tmux &> /dev/null
then
    sudo apt install --yes tmux
fi

# Print self IP address for remote connectivity
eth=eno1
ip -br -4 addr show dev $eth

#---------------------------------------------------------------------
# Follow instructions from
# https://github.com/zfsonlinux/zfs/wiki/Ubuntu-18.04-Root-on-ZFS

if ! grep -q universe /etc/apt/sources.list
then
    sudo apt-add-repository universe
    sudo apt update
fi

# Partition the disks.
disks=( /dev/disk/by-id/ata-ST6000VN0033-2EE110_???????? )
for disk in ${disks[*]}
do
    partitions=( $disk* )
    if [[ ${#partitions[*]} -eq 1 ]]
    then
	echo "Disk $disk has no partitions"
    elif [[ ${#partitions[*]} -eq 5 ]]
    then
	echo "Disk $disk has 4 partitions"
    else
	echo "Clearing partitions from disk $disk"
	sudo sgdisk --zap-all $disk
	echo "Adding BIOS boot partition"
	sudo sgdisk -a1 -n1:24K:+1000K -t1:EF02 $disk
	echo "Adding UEFI partition"
	sudo sgdisk     -n2:1M:+512M   -t2:EF00 $disk
	echo "Adding to boot pool"
	sudo sgdisk     -n3:0:+512M    -t3:BF01 $disk
	echo "Adding ZFS partition"
	sudo sgdisk     -n4:0:0        -t4:BF01 $disk
    fi
done

# Install ZFS and bootstrap packages.
if ! which zpool &> /dev/null
then
    sudo apt install --yes debootstrap gdisk zfs-initramfs
fi

# Create the boot pool.
if ! zpool list bpool &> /dev/null
then
    echo "Creating boot pool"
    sudo zpool create \
	 -o ashift=12 -d \
	 -o feature@async_destroy=enabled \
	 -o feature@bookmarks=enabled \
	 -o feature@embedded_data=enabled \
	 -o feature@empty_bpobj=enabled \
	 -o feature@enabled_txg=enabled \
	 -o feature@extensible_dataset=enabled \
	 -o feature@filesystem_limits=enabled \
	 -o feature@hole_birth=enabled \
	 -o feature@large_blocks=enabled \
	 -o feature@lz4_compress=enabled \
	 -o feature@spacemap_histogram=enabled \
	 -o feature@userobj_accounting=enabled \
	 -O acltype=posixacl \
	 -O canmount=off \
	 -O compression=lz4 \
	 -O devices=off \
	 -O normalization=formD \
	 -O relatime=on \
	 -O xattr=sa \
	 -O mountpoint=/ \
	 -R /mnt \
	 bpool \
	 raidz2 \
	 ${disks[*]/%/-part3}
else
    echo "Boot zpool already present"
fi

# Create the root pool.
if ! zpool list rpool &> /dev/null
then
    echo "Creating root pool"
    sudo zpool create \
	 -o ashift=12 \
	 -O acltype=posixacl \
	 -O canmount=off \
	 -O compression=lz4 \
	 -O dnodesize=auto \
	 -O normalization=formD \
	 -O relatime=on \
	 -O xattr=sa \
	 -O mountpoint=/ \
	 -R /mnt \
	 rpool \
	 raidz2 \
	 ${disks[*]/%/-part4}
else
    echo "Root zpool already present"
fi

# Create filesystem datasets to act as containers.
for dataset in rpool/ROOT bpool/BOOT
do
    if ! zfs list $dataset &> /dev/null
    then
	echo "Creating dataset $dataset"
	sudo zfs create \
	     -o canmount=off \
	     -o mountpoint=none \
	     $dataset
    else
	echo "Dataset already present $dataset"
    fi

    dataset+=/ubuntu
    if ! zfs list $dataset &> /dev/null
    then
	echo "Creating dataset $dataset"
	mountpoint=/
	case $dataset in
	    *BOOT*)
		mountpoint=/boot;;
	esac
	sudo zfs create \
	     -o canmount=noauto \
	     -o mountpoint=$mountpoint \
	     $dataset
    else
	echo "Dataset already present $dataset"
    fi

    if sudo zfs mount | grep -qw $dataset
    then
	echo "Dataset already mounted $dataset"
    else
	echo "Mounting dataset $dataset"
	if ! sudo zfs mount $dataset
	then
	    for vfs in dev proc sys
	    do
		if ! mount | grep -q /mnt/$vfs
		then
		    echo "Mounting /mnt/$vfs"
		    sudo mount --rbind /$vfs  /mnt/$vfs
		else
		    echo "Already mounted /mnt/$vfs"
		fi
	    done
	    sudo chroot /mnt mount $dataset
	fi
    fi
done

# Create daughter datasets.
create() {
    dataset=$1
    if [[ -z  $dataset ]]
    then
	echo >&2 "$FUNCNAME: Missing argument DATASET"
	return 1
    fi
    if zfs list $dataset &> /dev/null
    then
	echo "Dataset already present $dataset"
	return 0
    fi
    shift
    opts=$@
    echo "Creating dataset $dataset"
    sudo zfs create $opts $dataset
}
# Essential datasets.
create rpool/home
create rpool/home/root -o mountpoint=/root
create rpool/var       -o canmount=off
create rpool/var/lib   -o canmount=off
create rpool/var/log
create rpool/var/spool
# Optional datasets.
create rpool/var/cache -o com.sun:auto-snapshot=false
create rpool/var/tmp   -o com.sun:auto-snapshot=false
sudo chmod 1777 /mnt/var/tmp
create rpool/opt
create rpool/srv
create rpool/usr       -o canmount=off
create rpool/usr/local -o canmount=off
create rpool/var/mail
create rpool/var/snap
create rpool/var/www
create rpool/var/lib/AccountsService
create rpool/var/lib/docker -o com.sun:auto-snapshot=false
create rpool/var/lib/nfs    -o com.sun:auto-snapshot=false

# Install minimal system packges.
nfiles=$(sudo find /mnt -maxdepth 2 -type f | wc -l)
if [[ $nfiles -eq 0 ]]
then
    echo "Bootstrapping system"
    sudo debootstrap bionic /mnt
    sudo zfs set devices=off rpool
else
    echo "System already bootstrapped"
fi

# System configuration.
hostname=corelab2
fqdn=corelab2.mcb.uconn.edu
sudo sh -c "echo $hostname > /mnt/etc/hostname"
sudo sed -i -E \
     -e "s|(127.0.0.1.*localhost).*|\1 $hostname $fqdn|" \
     /mnt/etc/hosts
sudo sh -c "cat > /mnt/etc/netplan/01-netcfg.yaml <<EOF
network:
  version: 2
  ethernets:
    $eth:
      dhcp4: true
EOF"

# Configure package sources.
sudo sh -c "cat > /mnt/etc/apt/sources.list <<EOF
deb http://archive.ubuntu.com/ubuntu bionic main universe
deb-src http://archive.ubuntu.com/ubuntu bionic main universe

deb http://security.ubuntu.com/ubuntu bionic-security main universe
deb-src http://security.ubuntu.com/ubuntu bionic-security main universe

deb http://archive.ubuntu.com/ubuntu bionic-updates main universe
deb-src http://archive.ubuntu.com/ubuntu bionic-updates main universe
EOF"

# Enter chroot.
for vfs in dev proc sys
do
    if ! mount | grep -q /mnt/$vfs
    then
	echo "Mounting /mnt/$vfs"
	sudo mount --rbind /$vfs  /mnt/$vfs
    else
	echo "Already mounted /mnt/$vfs"
    fi
done

sudo chroot /mnt /bin/bash --login -x <<EOF
set -e -o pipefail

if ! [[ -L /etc/mtab ]]
then
    ln -s /proc/self/mounts /etc/mtab
fi

if ! [[ -f /var/cache/apt/pkgcache.bin ]]
then
    apt update
fi

if grep -q '#.* en_US' /etc/locale.gen
then
    sed -i '/^#.* en_US.* /s/^#//' /etc/locale.gen
    locale-gen
fi

echo America/New_York > /etc/timezone

install() {
    pkg=\$1
    shift
    opts=\$@
    if ! grep -q \$pkg <<< \$( dpkg -l )
    then
        apt install --yes \$opts \$pkg
    fi
}
install nano
install emacs25-nox
install linux-image-generic --no-install-recommends
install zfs-initramfs

# Workaround grub-pc forcing interactive install.
mnts=\$(printf ", %s" ${disks[*]})
mnts=\${mnts:1}
debconf-set-selections <( echo "grub-pc grub-pc/install_devices multiselect	\$mnts" )
install grub-pc

# UEFI booting.
install dosfstools
mkdir -p /boot/efi
if ! grep -q -E 'vfat|/boot/efi' <<< \$(lsblk -f $disks-part2)
then
    mkdosfs -F 32 -s 1 -n EFI $disks-part2
fi
if ! grep -q /boot/efi /etc/fstab
then
    echo PARTUUID=\$(blkid -s PARTUUID -o value $disks-part2) \
         /boot/efi vfat nofail,x-systemd.device-timeout=1 0 1 >> /etc/fstab
fi
if ! grep -q /boot/efi <<< \$(mount)
then
    mount /boot/efi
fi
install grub-efi-amd64-signed
install shim-signed

cat > /etc/systemd/system/zfs-import-bpool.service <<EEE
[Unit]
DefaultDependencies=no
Before=zfs-import-scan.service
Before=zfs-import-cache.service

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/sbin/zpool import -N -o cachefile=none bpool

[Install]
WantedBy=zfs-import.target
EEE
systemctl enable zfs-import-bpool.service

cp -a /usr/share/systemd/tmp.mount /etc/systemd/system/
systemctl enable tmp.mount

addgroup --system lpadmin
addgroup --system sambashare

if [[ -z "$skip" ]]
then
    grub-probe /boot
    update-initramfs -u -k all
fi

sed -i -E \
    -e 's|^(GRUB_CMDLINE_LINUX=).*|\1"root=ZFS=rpool/ROOT/ubuntu"|' \
    -e 's|^(GRUB_CMDLINE_LINUX_DEFAULT=.*)|#\1|' \
    -e 's|^(GRUB_TIMEOUT_STYLE=.*)|#\1|' \
    -e 's|^(GRUB_TIMEOUT=).*|\15|' \
    -e 's|^#(GRUB_TERMINAL=.*)|\1|' \
    /etc/default/grub

if [[ -z "$skip" ]]
then
    update-grub
fi

# Legacy BIOS booting.
if [[ -z "$skip" ]]
then
    for disk in ${disks[*]}
    do
        grub-install $disks
    done
fi

# UEFI.
if [[ -z "$skip" ]]
then
    grub-install \
        --target=x86_64-efi \
        --efi-directory=/boot/efi \
        --bootloader-id=ubuntu \
        --recheck \
        --no-floppy
fi

# Verify that the ZFS module is installed.
ls -1 /boot/grub/*/zfs.mod

# Fix filesystem mount ordering
umount /boot/efi
if ! grep -q bpool/BOOT/ubuntu /etc/fstab
then
    zfs set mountpoint=legacy bpool/BOOT/ubuntu
    echo bpool/BOOT/ubuntu /boot zfs \
         nodev,relatime,x-systemd.requires=zfs-import-bpool.service 0 0 >> /etc/fstab
fi
for path in /var/{log,spool,tmp}
do
    if ! grep -q rpool\$path /etc/fstab
    then
        zfs set mountpoint=legacy rpool\$path
        echo rpool\$path \$path zfs nodev,relatime 0 0 >> /etc/fstab
    fi
done

if ! [[ -d /.zfs/snapshot/install ]]
then
    for dataset in bpool/BOOT rpool/ROOT
    do
        zfs snapshot \$dataset/ubuntu@install
    done
fi
EOF

# Unmount filesystems.
mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | xargs -i{} sudo umount -lf {}
sudo zpool export -a
