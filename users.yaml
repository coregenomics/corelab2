---
- name: 'Create all users and ZFS directories'
  hosts: 'all'
  # Gather ansible_user_id.
  gather_facts: yes
  vars:
    users:
      - des20008
      - gjv11003
      - ket22003
      - law07007
      - ljc13007
      - lmw09001
      - pan14001
      - rsd16103
      - scp07005
      - sjw17003
    snapshots:
      - name: keep hourly snapshot for 1 week.
        special_time: hourly
        ttl: 1w
      - name: keep weekly snapshot for 1 month.
        special_time: weekly
        ttl: 1m

  tasks:

  # https://askubuntu.com/a/1039368
  - name: 'Disable gnome-initial-setup'
    become: yes
    ini_file:
      path: '/etc/gdm3/custom.conf'
      section: 'daemon'
      option: 'InitialSetupEnable'
      value: 'false'
      no_extra_spaces: yes
    tags: 'gnome-initial-setup'

  - name: 'Install package for ZFS snapshots'
    become: yes
    apt:
      name: zfsnap

  - name: 'Create local user and group'
    become: true
    user:
      name: '{{ item }}'
      create_home: false
      shell: '/bin/bash'
    loop: '{{ users }}'

  - name: 'Create ZFS dataset'
    become: true
    zfs:
      name: 'rpool/home/{{ user }}'
      state: 'present'
    loop: '{{ users }}'
    loop_control:
      loop_var: 'user'
    tags: 'zfs'

  - name: 'Set home directory ownership'
    become: true
    file:
      name: '/home/{{ user}}'
      owner: '{{ user }}'
      group: '{{ user }}'
    loop: '{{ users }}'
    loop_control:
      loop_var: 'user'
    tags: 'chown'

  - name: 'Add user to groups and generate SSH key'
    become: true
    user:
      name: '{{ user }}'
      groups:
        - adm
        - lpadmin
        - plugdev
        - audio
        - video
        - sambashare
        - sudo
        - users
      generate_ssh_key: yes
    loop: '{{ users }}'
    loop_control:
      loop_var: 'user'
    tags: 'groups'

  - name: 'Populate skeleton files'
    become: yes
    copy:
      backup: yes
      force: no
      mode: 'preserve'
      owner: '{{ user }}'
      group: '{{ user }}'
      src: '/etc/skel/'
      dest: '/home/{{user}}/'
    loop: '{{ users }}'
    loop_control:
      loop_var: 'user'
    tags: 'skel'

  - name: 'Set ~/.emacs.d/ permissions'
    become: yes
    file:
      name: '/home/{{user}}/.emacs.d'
      mode: 0700
    loop: '{{ users }}'
    loop_control:
      loop_var: 'user'
    tags: 'skel'

  - name: 'Schedule snaption cleanup'
    become: yes
    cron:
      cron_file: 'zfsnap'
      user: 'root'
      name: 'Delete expired snapshots'
      job: '/usr/sbin/zfSnap -d'
      special_time: 'daily'

  - name: 'Itemize home directories'
    set_fact:
        dirs: "{{ dirs | default([]) + [{'dataset': 'rpool' + '/home/' + item}] }}"
    with_items:
      - '{{ users }}'

  - name: 'Schedule spanshot creation'
    become: yes
    cron:
      cron_file: 'zfsnap'
      user: 'root'
      name: '{{ item.0.dataset }} {{ item.1.name }}'
      job: '/usr/sbin/zfSnap -z -s -S -a {{ item.1.ttl }} {{ item.0.dataset }}'
      special_time: '{{ item.1.special_time }}'
    loop: '{{ dirs | product(snapshots) | list }}'
